
$(function () {
    //Initialization of HTML button and it's textarea.
    $('#button1').addClass('selected');
    var counter = 1;
    var btnArray = ['1'];
    const editorWidth = 100;
    let time = 4000;

    let timer = setInterval(() => {
        if (time > 0) {
            time = time - 1000;
        }
    }, 1000);

    $('#textarea1').css('width', '100%');
    $('#textarea2').css('display', 'none');
    $('#textarea3').css('display', 'none');
    $('#textarea4').css('display', 'none');

    //Button click event
    $('input[type="button"]').click(function () {

        if ($(this).hasClass('selected')) {
            if (counter > 1) {
                $(this).removeClass('selected');
            }
        } else {
            $(this).addClass('selected');
        }

        // Counts of selected buttons and displaying/ hiding textareas for every button
        Array.from($('.menu-bar').children()).forEach(button => {

            const buttonid = $(button).attr('id').replace('button', '');

            if ($(button).hasClass('selected')) {
                const indexToAdd = btnArray.findIndex(x => x === buttonid);
                $(`#textarea${buttonid}`).css('display', '')
                if (indexToAdd === -1) {
                    btnArray.push(buttonid);
                    counter++;
                }

            } else {
                $(`#textarea${buttonid}`).css('display', 'none');
                const indexToRemove = btnArray.findIndex(x => x === buttonid);
                if (indexToRemove > -1) {
                    btnArray.splice(indexToRemove, 1);
                    counter--;
                }

            }
        });
        // for assigning width to editors dynamically
        Array.from($('.editor').children()).forEach(ta => {
            if (btnArray.indexOf($(ta).attr('id').replace('textarea', '')) > -1) {
                $(ta).css('width', `${editorWidth / btnArray.length}%`);
            }
        });
    });

    $('#textarea1').keyup(function (event) {
        $('#textarea4').contents().find('html').html(event.target.value);
        // document.getElementById('textarea4').src = "data:text/html;charset=utf-8," + escape(event.target.value);
    });
    $('#textarea2').keyup(function (event) {
        $('#textarea4').contents().find('head').append(`<style>${event.target.value}</style>`);
    });

    $('#textarea3').keyup(function (event) {
        if (time <= 0) {
            setTimeout(() => {
                time = 4000;
                document.getElementById('textarea4').contentWindow.eval(event.target.value);
            }, time);
        }
    });
    
    $('#hamburger-icon').click(function () {
        if ($('.menu-header').css('position') === 'sticky') {
        $('.menu-header').animate({
            marginTop: '-59px',
            top: ''
        }, 400, function() {
            $(this).css('position', 'relative');
            $(this).css('display', 'block');
        });
        $('.menu-bar').animate({
            display: 'block',
            marginLeft: '500px',
            marginTop: '-50px'
        });
        $('.toggleMenu').animate({
            position: 'relative',
            top: '0px'
        });
        
    } else {
        $('.menu-header').animate({
            marginTop: '',
            top: '0px'
        }, 400, function() {
            $(this).css('position', 'sticky');
            $(this).css('display', 'inline-flex');
        });
        $('.menu-bar').animate({
            display: 'flex',
            marginLeft: '350px',
            marginTop: ''
        });
        $('.toggleMenu').animate({
            position: 'sticky',
            top: '60px'
        });
    }
    });
});